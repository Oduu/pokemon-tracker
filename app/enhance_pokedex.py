import requests
import pprint
import json

poke = {}

with open('pokedex2.json') as f:
    poke = json.load(f)

URL = "https://pokeapi.co/api/v2/pokemon/{}/encounters"

i=0
for p in poke:
	url = URL.format(p['id'].lstrip("0"))
	get_data = requests.get(url)
	p['locations'] = {}
	for location in get_data.json():
		name = location['location_area']['name']
		game = location['version_details'][0]['version']['name']
		if game not in p['locations']:
			p['locations'][game] = []

		p['locations'][game].append(name)

	i += 1
	print(i)

with open('pokedex2.json', 'w') as outfile:
	json.dump(poke, outfile)