people = [
	{"id": "001", "name": "bulbasaur"},
	{"id": "004", "name": "squirtle"}
]

for p in people:
	print(p)
	print(p["id"])
	print("---------------")

print("===========")

for i, p in enumerate(people):
	print(i)
	print(p)
	print("------------")
