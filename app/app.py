import json
from flask import Flask, render_template, request
app = Flask(__name__)

DATA = []
region_list = ["Kanto", "Johto", "Hoenn", "Sinnoh", "Unova", "Kalos", "Alola", "Galar"]
with open('pokedex2.json') as f:
    DATA = json.load(f)

@app.route("/")
@app.route("/gen/<gen>")
def hello(gen=None):
    searchCriteria = request.args.get('query')
    print("The search Criteria is " + str(searchCriteria))
    pokemons = []
    if gen is not None and int(gen) < 9 and searchCriteria is None:
        region = "{}".format(region_list[int(gen)-1])
        for d in DATA:
            if str(d["generation"]) == gen:
                pokemons.append(d)
    elif searchCriteria == "":
        pokemons = DATA
        region = "All Generations"
    elif searchCriteria is not None:
        region = "Search Result"
        for d in DATA:
            if searchCriteria.upper() in str(d["name"]["english"]).upper():
                pokemons.append(d)
            elif searchCriteria.upper() in str(d["type"]).upper():
                pokemons.append(d)
    else:
        pokemons = DATA
        region = "All Generations"

    return render_template('index.html', region=region, pokemons=pokemons, region_list=region_list, searchCriteria=searchCriteria)


app.debug = True
app.run(host="0.0.0.0")



