#Add generations
import json
from pprint import pprint

with open('/home/er/Desktop/pokemon-tracker/app/static/pokedex.json') as f:
	data = json.load(f)

for d in data:
	if d["id"] <= 151:
		d["generation"] = 1
	elif d["id"] <= 251:
		d["generation"] = 2
	elif d["id"] <= 386:
		d["generation"] = 3
	elif d["id"] <= 493:
		d["generation"] = 4
	elif d["id"] <= 649:
		d["generation"] = 5
	elif d["id"] <= 721:
		d["generation"] = 6
	elif d["id"] <= 809:
		d["generation"] = 7
		
	d["id"] = str(d["id"]).rjust(3, "0")
	del d["name"]["japanese"]
	del d["name"]["chinese"]

with open('pokedex2.json', 'w') as outfile:
	json.dump(data, outfile)

