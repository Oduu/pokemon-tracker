import urllib.request

START = 722
END = 810

URL = "https://www.serebii.net/pokemon/art/{}.png"

for id in range(START, END):
  urllib.request.urlretrieve(URL.format(id), "{}.png".format(id))