import os, sys

# Save as ID.png
i = 1
for filename in os.listdir("."):
    if "png" not in filename:
        continue

    os.rename(filename, "{}.png".format(i))
    i += 1

# Pad with zero
for filename in os.listdir("."):
    if "png" not in filename:
        continue

    new = filename.rjust(7, "0")
    os.rename(filename, new)