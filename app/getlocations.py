import requests
import pprint
import json

poke = {}

URL = "https://pokeapi.co/api/v2/pokemon/{}/encounters"

poke = {9: {"platinum":["pooforest"]}}


for i in range(9,11):
	url = URL.format(i)
	get_data = requests.get(url)
	poke[i] = {}
	for location in get_data.json():
		name = location['location_area']['name']
		game = location['version_details'][0]['version']['name']
		if game not in poke[i]:
			poke[i][game] = []

		poke[i][game].append(name)

	print(i)

print(poke)

with open('locations.json', 'w') as outfile:
	json.dump(poke, outfile)